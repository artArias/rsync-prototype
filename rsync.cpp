#include <iostream>
#include <unistd.h>
#include <stdlib.h>
#include <vector>
#include <stdio.h>
#include <dirent.h>
#include <sstream>
#include <thread>
#include <inttypes.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <stdexcept>
#include "rsync.h"

using namespace std;

void read_fl(int fd, char* position, size_t need_to_read) {
    size_t was_read = 0;
    while (true) {
    	int read_now = read(fd, position + was_read, need_to_read);
        need_to_read -= read_now;
        was_read += read_now;
        if (read_now < 0) {
        	throw runtime_error("Error. Can not read the frame");
        }
        if (read_now == 0) {
            break;
        }
    }
}

void write_fl(int fd, char* position, size_t need_to_write) {
    size_t was_written = 0;
    while (true) {
    	int written_now = write(fd, position + was_written, need_to_write);
        need_to_write -= written_now;
        was_written += written_now;
        if (written_now < 0) {
        	throw runtime_error("Error. Can not write the frame");
        }
        if (written_now == 0) {
            break;
        }
    }
}

void SocketConnection::ReadFrame(Frame* frame) {
    int len;
    read_fl(fd_, (char*)&len, 4);
    uint8_t msg_id;
    read_fl(fd_, (char*)&msg_id, 1);
    frame->msg_id = MsgId(msg_id);
    frame->body.resize(len);
    read_fl(fd_, (char*)frame->body.c_str(), len);
}

void SocketConnection::WriteFrame(Frame* frame) {
    int len = frame->body.length();
    write_fl(fd_, (char*)&len, 4);
    int id = static_cast<int> (frame->msg_id);
    write_fl(fd_, (char*)&id, 1);
    write_fl(fd_, (char*)frame->body.c_str(), len);
}

FileList GetFileList(string dir) {
    FileList list;
    vector<string> vec;
    struct dirent *ent;
    const char* n_str = dir.c_str();
    DIR *path;
    if (opendir(n_str) == NULL) {
        throw logic_error("Error. Dir not found");
    }
    path = opendir(n_str);
    if (path) {
        while ((ent = readdir(path)) != NULL) {
            vec.push_back(ent->d_name);
        }
    }
    if (closedir(path) == -1) {
        throw runtime_error("Error. Dir can not be closed");
    }
    list.files = vec;
    return list;
}

void Protocol::SendGetList(const string destination) {
    Frame f;
    string dest_path(destination.c_str());
    f.msg_id = MsgId::GETLIST;
    f.body = dest_path;
    conn_->WriteFrame(&f);
}

void Protocol::SendFileList(const FileList& list) {
    Frame f;
    stringstream ss;
    boost::archive::text_oarchive archive(ss);
    archive << list;
    f.body = ss.str();
    f.msg_id = MsgId::FILELIST;
    conn_->WriteFrame(&f);
}

void Protocol::success() {
    Frame f;
    f.msg_id = MsgId::OK;
    f.body = "";
    conn_->WriteFrame(&f);
}

FileList Protocol::ReceiveFileList() {
    stringstream ss;
	FileList list;
    ss << last_received_.body;
    boost::archive::text_iarchive archive(ss);
    archive >> list;
    return list;
}

MsgId Protocol::RecvMsg() {
    conn_->ReadFrame(&last_received_);
    return last_received_.msg_id;
}

vector<string> cp_fl_calc(vector<string> source, vector<string> destination) {
    vector<string> cp;
    for (int i = 0; i < source.size(); ++i) {
        for (int j = 0; j < destination.size(); ++j) {
            if ((j == destination.size() - 1) && (source[i] != destination[j])) {
                cp.push_back(source[i]);
            } else if (source[i] == destination[j]) {
                break;
            }
        }
    }
    return cp;
}

vector<string> result_act(vector<string> cp, vector<string> rm) {
    vector<string> result;
    for (size_t i = 0; i < rm.size(); ++i) {
        if (rm[i] != "." || rm[i] != "..") {
            result.push_back("rm " + rm[i]);    
        }
    }
    for (size_t i = 0; i < cp.size(); ++i) {
        if (cp[i] != "." || cp[i] != "..") {
            result.push_back("cp " + cp[i]);
        }
    }
    return result;
}

int Sender::start() {
    SendGetList(destination_);
    while (true) {
        RecvMsg();
        if (last_received_.msg_id == MsgId::FILELIST) {
            FileList dest_files = ReceiveFileList();
            FileList source_files = GetFileList(source_);
            vector<string> res = result_act(cp_fl_calc(source_files.files,dest_files.files),
                cp_fl_calc(dest_files.files, source_files.files));
            for (int i = 0; i < res.size(); ++i) {
                cout << res[i] << endl;
            }
            success();
            break;
        }
    }
    return 0;
}

int Receiver::start() {
    while (true) {
        RecvMsg();
        if (last_received_.msg_id == MsgId::GETLIST) {
            FileList files = GetFileList(last_received_.body);
            SendFileList(files);
        }
        success();
        break;
    }
    return 0;
}
