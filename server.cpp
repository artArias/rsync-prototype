#include <iostream>
#include <unistd.h>
#include <stdlib.h>
#include <vector>
#include <stdio.h>
#include <dirent.h>
#include <sstream>
#include <thread>
#include <inttypes.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <stdexcept>
#include <string>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <cstdint>
#include <boost/program_options/parsers.hpp>
#include <boost/program_options/variables_map.hpp>
#include <boost/program_options/options_description.hpp>
#include "rsync.h"

using namespace std;
namespace po = boost::program_options;

void sender_run(int sockt, string source, string destination) {
    SocketConnection s(sockt);
    Sender sender(&s, source, destination);
    std::thread t1([&] {
        sender.start();
    });
    t1.join();
}

void receiver_run(int sockt) {
    SocketConnection r(sockt);
    Receiver receiver(&r);
    std::thread t2([&] {
        receiver.start();
    });
    t2.join();
}

class Server{
private:
    int port, sock_fd;
    string source, destination;
public:
    Server(int port_, string source_, string destination_) {
        port = port_;
        source = source_;
        destination = destination_;
        int opt = 1;
        sock_fd = socket(AF_INET, SOCK_STREAM, 0);
        setsockopt(sock_fd, SOL_SOCKET, SO_REUSEADDR, &opt, sizeof(opt));
        if (sock_fd < 0) {
            throw runtime_error("Socket error");
        }
    }

    void srv_listen() {
        struct sockaddr_in server;
        server.sin_family = AF_INET;
        server.sin_addr.s_addr = INADDR_ANY;
        server.sin_port = htons(port);
        if(bind(sock_fd, (struct sockaddr *)&server, sizeof(server)) < 0) {
            close(sock_fd);
            throw runtime_error("Error. Bind failed");
        }
        if (listen(sock_fd, 1) < 0) {
            throw runtime_error("Error. Listen failed");
        }
    }

    void answer() {
        int client_sock, size;
        struct sockaddr_in client;
        size = sizeof(struct sockaddr_in);
        client_sock = accept(sock_fd, (struct sockaddr *)&client, (socklen_t*)&size);
        if (client_sock < 0) {
            throw std::runtime_error("Error. Server connection failed");
        }
        sender_run(client_sock, source, destination);
        close(client_sock);
    }
    ~Server(){
        close(sock_fd);
    }
};

int client_run(int port) {
    int sock;
    const char* adress = "127.0.0.1";
    struct sockaddr_in server;
    sock = socket(AF_INET , SOCK_STREAM , 0);
    if (sock < 0) {
        throw runtime_error("Error. Can not create socket");
    }
    server.sin_addr.s_addr = inet_addr(adress);
    server.sin_family = AF_INET;
    server.sin_port = htons(port);
    if (connect(sock, (struct sockaddr *)&server , sizeof(server)) < 0) {
        throw runtime_error("Error. Connection failed");
    }
    receiver_run(sock);
    close(sock);
}

int main (int argc, char* argv[]) {
    po::options_description desc("Allowed options");
    desc.add_options()
    ("help", "produce help message")
        ("sender", po::value<string>(), "sender run")
        ("receiver", "receiver run")
        ("port", po::value<int>(), "server port option")
    ;

    po::variables_map send, rec, port;
    po::store(po::parse_command_line(argc, argv, desc), send);
    po::notify(send);
    po::store(po::parse_command_line(argc,argv, desc), rec);
    po::notify(rec);
    po::store(po::parse_command_line(argc,argv, desc), rec);
    po::notify(port);

    if (send.count("help")) {
        cout << desc << "\n";
        return 1;
    }

    if (send.count("sender")) {
        if (isdigit(argv[2][0])) {
            Server Server(atoi(argv[2]), argv[4], argv[5]);
            Server.srv_listen();
            Server.answer();
        } else {
            Server Server(atoi(argv[5]), argv[2], argv[3]);
            Server.srv_listen();
            Server.answer();
        }
    }

    if (rec.count("receiver")) {
        if (isdigit(argv[3][0])) {
            client_run(atoi(argv[3]));
        } else {
            client_run(atoi(argv[2]));
        }
    }
}
