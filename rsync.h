#pragma once
#include <utility>
#include <vector>
#include <string>
#include <boost/serialization/string.hpp>
#include <boost/serialization/vector.hpp>
#include <boost/archive/text_iarchive.hpp>
#include <boost/archive/text_oarchive.hpp>

enum class MsgId {
    GETLIST = 1,
    FILELIST = 2,
    REMOVE = 3,
    FILE = 4,
    FILE_PART = 5,
    OK = 6
};

struct Frame {
    MsgId msg_id;
    std::string body;
};

class Connection {
public:
    virtual ~Connection() {}
    virtual void WriteFrame(Frame* frame) = 0;
    virtual void ReadFrame(Frame* frame) = 0;
};

class SocketConnection {
public:
    SocketConnection(int fd) : fd_(fd) {}
    virtual void WriteFrame(Frame* frame);
    virtual void ReadFrame(Frame* frame);
private:
    int fd_;
};

struct FileList {
    std::vector<std::string> files;
    template<class Archive>
    void serialize(Archive& ar, const unsigned int version) {
        ar & files;
    }
};

class Protocol {
public:
    Protocol(SocketConnection* conn) : conn_(conn) {}
    void SendGetList(const std::string destination);
    void SendFileList(const FileList& list);
    void success();
    MsgId RecvMsg();
    FileList ReceiveFileList(); // reads next frame
    ~Protocol() {}
protected:
    SocketConnection* conn_;
    Frame last_received_;
};

class Sender : public Protocol {
public:
    Sender(SocketConnection* conn, std::string source, std::string destination) : Protocol(conn) {
        source_.resize(source.length());
        destination_.resize(destination.length());
        source_ = source;
        destination_ = destination;
    }
    int start();
private:
    std::string source_;
    std::string destination_;
};

class Receiver : public Protocol {
public:
    Receiver(SocketConnection* conn) : Protocol(conn) {
        destination_ = "";
    }
    int start();
private:
    std::string destination_;    
};

FileList GetFileList(std::string path);
std::vector<std::string> cp_fl_calc(std::vector<std::string> source,std::vector<std::string> destination);
std::vector<std::string> result_act(std::vector<std::string> cp, std::vector<std::string> rm);
